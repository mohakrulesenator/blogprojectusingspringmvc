package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.repo.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BlogServiceImpl implements BlogService {

    private BlogRepository blogRepo;
    @Autowired
    public BlogServiceImpl(BlogRepository blogRepository) {
        blogRepo = blogRepository;
    }


    public Page<Blog> listAllBlogs(int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return (Page<Blog>)blogRepo.findByIsPublishedTrue(sortBycreatedOn);
    }

    @Override
    public Page<Blog> listAllBlogsByTitle(String search, int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return (Page<Blog>)blogRepo.findByIsPublishedTrueAndTitleLike(search,sortBycreatedOn);
    }

    @Override
    public Page<Blog> listAllBlogsByContent(String search, int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return (Page<Blog>)blogRepo.findByIsPublishedTrueAndContentLike(search,sortBycreatedOn);
    }


    @Override
    public Page<Blog> listAllBlogsByTags(boolean isPublished, String search, int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return (Page<Blog>)blogRepo.findBytagsLike(isPublished,search,sortBycreatedOn);
    }

    @Override
    public Page<Blog> findBlogsByUserAndIsPublishedTrue(User user,int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return (Page<Blog>)blogRepo.findBlogsByUserAndIsPublishedTrue(user,sortBycreatedOn);
    }

    @Override
    public Page<Blog> findBlogsByUserAndIsPublishedFalse(User user, int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return blogRepo.findBlogsByUserAndIsPublishedFalse(user,sortBycreatedOn);
    }

    @Override
    public Page<Blog> findBlogByUser(User user,int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return blogRepo.findBlogByUser(user,sortBycreatedOn);
    }

    @Override
    public void deleteAllByUser(User user) {
        blogRepo.deleteAllByUser(user);
    }

    @Override
    public Page<Blog> listAllDrafts(int page) {
        Pageable sortBycreatedOn = PageRequest.of(page,2,Sort.by(Sort.Direction.DESC,"createdOn"));
        return blogRepo.findByIsPublishedFalse(sortBycreatedOn);
    }


    @Override
    public List<Blog> findAll() {
        return (List<Blog>) blogRepo.findAll();
    }

    @Override
    public Blog findById(int theId) {
        Optional<Blog> result = blogRepo.findById(theId);

        Blog theBlog = null;

        if (result.isPresent()) {
            theBlog = result.get();
        }
        else {
            throw new RuntimeException("Blog with this id - " + theId + "is not found");
        }

        return theBlog;
    }



    @Override
    public void save(Blog theBlog) {
        blogRepo.save(theBlog);
    }

    @Override
    public void deleteById(int theId) {
        blogRepo.deleteById(theId);
    }
}
