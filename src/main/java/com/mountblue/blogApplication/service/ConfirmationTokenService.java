package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.ConfirmationToken;
import org.springframework.stereotype.Service;


public interface ConfirmationTokenService {

    public void save(ConfirmationToken confirmationToken);

    public ConfirmationToken findByconfirmationToken(String confirmationToken);

    public void deleteById(long theId);
}
