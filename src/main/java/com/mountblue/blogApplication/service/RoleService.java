package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Role;

import java.util.List;

public interface RoleService {

    public List<Role> findAll();

    public void save(Role role);

    public Role findByRole(String role);

}
