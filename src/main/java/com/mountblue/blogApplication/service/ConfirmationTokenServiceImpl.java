package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.ConfirmationToken;
import com.mountblue.blogApplication.repo.ConfirmationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    @Autowired
    ConfirmationTokenRepository confirmationTokenRepo;


    public void save(ConfirmationToken confirmationToken) {
        confirmationTokenRepo.save(confirmationToken);
    }


    public ConfirmationToken findByconfirmationToken(String confirmationToken) {
        return confirmationTokenRepo.findByconfirmationToken(confirmationToken);
    }

    @Override
    public void deleteById(long theId) {
        confirmationTokenRepo.deleteById(theId);
    }


}
