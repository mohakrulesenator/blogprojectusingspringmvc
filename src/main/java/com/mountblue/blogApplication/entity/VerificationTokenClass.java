package com.mountblue.blogApplication.entity;


import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="VERIFICATION_TOKEN")
public class VerificationTokenClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="V_TOKEN_ID")
    private long verificationTokenId;

    @Column(name="verification_token")
    private String verificationToken;

    public long getVerificationTokenId() {
        return verificationTokenId;
    }

    public void setVerificationTokenId(long tokenId) {
        this.verificationTokenId = tokenId;
    }

    public String getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToOne(targetEntity = User.class,cascade = {CascadeType.PERSIST})
    @JoinColumn(name="V_USER_ID",nullable=false)
    private User user;

    public VerificationTokenClass(User user) {
        this.user = user;
        this.verificationToken= UUID.randomUUID().toString();
    }

    public VerificationTokenClass(){

    }

}
