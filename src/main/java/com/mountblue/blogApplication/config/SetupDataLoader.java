package com.mountblue.blogApplication.config;

import com.mountblue.blogApplication.controller.UserController;
import com.mountblue.blogApplication.entity.Role;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.service.RoleServiceImpl;
import com.mountblue.blogApplication.service.userCredService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;
    private userCredService userServiceImp;
    private RoleServiceImpl roleServiceImp;
    private PasswordEncoder bCryptPasswordEncoder;
    Logger logger = LoggerFactory.getLogger(SetupDataLoader.class);
    public SetupDataLoader(userCredService userServiceImp, RoleServiceImpl roleServiceImp, PasswordEncoder
            bCryptPasswordEncoder) {
        this.userServiceImp = userServiceImp;
        this.roleServiceImp = roleServiceImp;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup) {
            return;
        }
        Role roleAdmin = createRoleIfNotFound("ROLE_ADMIN");
        Role roleUser = createRoleIfNotFound("ROLE_USER");
        createUserIfNotFound("mohak.bhatnagar@mountblue.tech", "mohak", "bhatnagar", "mohak","9874563210","mohak", roleAdmin);
        alreadySetup = true;
    }
    @Transactional
    Role createRoleIfNotFound(final String name) {
        logger.info("Inside role method "+name);
        System.out.println("INSIDE ROLE METHOD");
        Role role = roleServiceImp.findByRole(name);
        if (role == null) {
            logger.info("role is null");
            role = new Role();
            role.setRole(name);
            roleServiceImp.save(role);
        }
        return role;
    }
    @Transactional
    void createUserIfNotFound(final String email, String firstName,
                              String LastName, String password, String contact, String username,
                              Role role) {
        if (!userServiceImp.findByUsername(username).isPresent()) {
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(LastName);
            user.setEmail(email);
            user.setContact(contact);
            user.setUsername(username);
            user.setActive(true);
            user.setPassword(bCryptPasswordEncoder.encode(password));
            user.setRole(role);
            userServiceImp.save(user);
        }
    }
}