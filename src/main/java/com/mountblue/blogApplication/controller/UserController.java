package com.mountblue.blogApplication.controller;

import com.mountblue.blogApplication.entity.ConfirmationToken;
import com.mountblue.blogApplication.entity.Role;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.entity.VerificationTokenClass;
import com.mountblue.blogApplication.service.*;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    @Autowired
    private ConfirmationTokenServiceImpl confirmationTokenService;

    @Autowired
    private VerificationTokenServiceImpl verificationTokenService;

    @Autowired
    public UserController(@Qualifier("userCredService") UserService theUserService) {userService = theUserService; }

    @Autowired
    public RoleServiceImpl roleService;

    @Autowired
    public EmailService emailService;

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);

    @GetMapping("/signUp")
    public String signUp(Model theModel, Principal principal) {
        if(principal != null) {
            return "redirect:/";
        }
        User user = new User();
        theModel.addAttribute("user",user);
        return "signUp";
    }

    @GetMapping("/login")
    public String login() {
        return "signIn";
    }


    @PostMapping("/saveUser")
    public String saveUser(Model theModel, @Valid @ModelAttribute User newUser, Errors errors,HttpServletRequest request) throws IOException {

        if(errors.hasErrors()) {
            return "signUp";
        }

        User existingUser = userService.findByemailIgnoreCase(newUser.getEmail());
        if(existingUser != null) {
            logger.error("USER ALREADY EXISTS");
            theModel.addAttribute("userExists",true);
            return "signUp";
        }
        else {
            ConfirmationToken confirmationToken = new ConfirmationToken(newUser);
            logger.info(confirmationToken.getUser().getUsername());
            confirmationTokenService.save(confirmationToken);
            String subject = "Complete Registration !";
            String body = "To confirm your account, please click here : "
                    + "http://" + request.getServerName() + ":" + request.getServerPort() + "/confirm-account?token=" + confirmationToken.getConfirmationToken();
            emailService.sendEmail(newUser.getEmail(),subject,body);
            logger.info(confirmationToken.getConfirmationToken());
            logger.info("MAIL SENT PLEASE CHECK YOUR ACCOUNT");
            theModel.addAttribute("success","Please check your inbox for confirming your account.");
            return "signUp";
        }
    }
    @GetMapping("/confirm-account")
    public String confirmUserAccount(@RequestParam("token")String confirmationToken) {

        ConfirmationToken token = confirmationTokenService.findByconfirmationToken(confirmationToken);
        logger.info("Token received is"+token);

        if(token != null) {
            User user = userService.findByemailIgnoreCase(token.getUser().getEmail());
            user.setActive(true);
            user.setPassword(encoder.encode(user.getPassword()));
            Role role = roleService.findByRole("ROLE_USER");
            user.setRole(role);
            userService.save(user);
            logger.info("USER VERIFIED");
            long token_id = token.getTokenid();
            confirmationTokenService.deleteById(token_id);
        }
        else{
            logger.error("INVALID USER");
        }
        return "successRegistration";
    }

    @GetMapping("/forgotPassword")
    public String forgotPassword() {
        return "forgotPassword";
    }

    @PostMapping("/sendVerificationToken")
    public String sendVerificationToken(@RequestParam(value="email") String email, Model theModel, HttpServletRequest request) throws IOException {
        User user = userService.findByemailIgnoreCase(email);
        if(user != null) {
            logger.info("email is "+user.getUsername());
            VerificationTokenClass verificationToken = new VerificationTokenClass(user);
            verificationTokenService.save(verificationToken);

            String subject = "Complete Password reset !";
            String body = "To reset your password, please click here : "
                    + "http://" + request.getServerName() + ":" + request.getServerPort() + "/confirm-reset?token=" + verificationToken.getVerificationToken();
            emailService.sendEmail(user.getEmail(),subject,body);
            logger.info("MAIL SENT PLEASE CHECK YOUR ACCOUNT");
            theModel.addAttribute("emailDoesNotExist",false);
            return "forgotPassword";
        }
        else{
            logger.error("EMAIL DOES NOT EXIST");
            theModel.addAttribute("emailDoesNotExist",true);
            return "forgotPassword";
        }
    }

    @GetMapping("/confirm-reset")
    public String resetPassword(@RequestParam(value="token")String verificationToken,Model theModel){
        VerificationTokenClass token = verificationTokenService.findByVerificationToken(verificationToken);
        if(token != null) {
            User user = token.getUser();
            long token_id = token.getVerificationTokenId();
            verificationTokenService.deleteById(token_id);
            user.setActive(true);
            userService.save(user);
            theModel.addAttribute("user",user);
            return "resetPassword";
        }
        else{
            System.out.println("This link is invalid or broken");
            return "signIn";
        }

    }

    @PostMapping("/reset-Password")
    public String resetUser(User user) {
        System.out.println(user.getEmail());
        User tokenUser = userService.findByemailIgnoreCase(user.getEmail());
        tokenUser.setPassword(encoder.encode(user.getPassword()));
        userService.save(tokenUser);
        return "successRegistration";
    }





}
