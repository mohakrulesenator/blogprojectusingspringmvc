package com.mountblue.blogApplication.controller;

import com.mountblue.blogApplication.entity.Role;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.service.BlogService;
import com.mountblue.blogApplication.service.CommentServiceImpl;
import com.mountblue.blogApplication.service.RoleServiceImpl;
import com.mountblue.blogApplication.service.userCredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class adminController {
    @Autowired
    userCredService userServiceImpl;
    @Autowired
    RoleServiceImpl roleService;
    @Autowired
    private BlogService blogService;
    @Autowired
    private CommentServiceImpl commentService;

    @GetMapping("")
    public String getAdminData(Model theModel) {
        List<User> listAllUser = userServiceImpl.findAll();
        theModel.addAttribute("listAllusers",listAllUser);
        return "adminPage";
    }

    @GetMapping("/changeRole")
    public String changeRole(@RequestParam(value="username")String username,Model theModel) {
        List<User> listAllUser = userServiceImpl.findAll();
        theModel.addAttribute("listAllusers",listAllUser);
        User user = userServiceImpl.findByUsername(username).get();
        if(user.getRole().getRole().contains("USER")) {
            Role role = roleService.findByRole("ROLE_ADMIN");
//            System.out.println(role.getRole());
            user.setRole(role);
            roleService.save(role);
        }
        else {
            Role role = roleService.findByRole("ROLE_USER");
            user.setRole(role);
            roleService.save(role);
        }
        return "adminPage";
    }
    @Transactional
    @GetMapping("/deleteUser")
    public String deleteUser(@RequestParam(value="username")String username,Model theModel) {
        List<User> listAllUser = userServiceImpl.findAll();
        theModel.addAttribute("listAllusers",listAllUser);
        User user = userServiceImpl.findByUsername(username).get();
        commentService.deleteAllByUser(user);
        blogService.deleteAllByUser(user);
        userServiceImpl.deleteById(user.getId());
        return "adminPage";
    }


}
