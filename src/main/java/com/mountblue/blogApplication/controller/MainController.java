package com.mountblue.blogApplication.controller;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Comment;
import com.mountblue.blogApplication.entity.Tag;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Controller
@RequestMapping("/")
public class MainController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    private BlogService blogService;

    @Autowired
    public MainController(BlogServiceImpl theBlogService) {
        blogService = theBlogService;
    }

    @Autowired
    userCredService userServiceImpl;

    @Autowired
    TagServiceImpl tagService;

    @Autowired
    private CommentServiceImpl commentService;


    @GetMapping("/createBlog")
    public String createBlog(Model theModel,Principal principal) {
        if(principal == null){
            return "signIn";
        }
        Blog newBlog = new Blog();
        User author = userServiceImpl.findByUsername(principal.getName()).get();
//        theModel.addAttribute("loggedIn",principal);
        theModel.addAttribute("author",author.getUsername());
        theModel.addAttribute("blog",newBlog);

        return "createBlog";
    }

    @GetMapping("/")
    public String displayGlobalFeed(Model theModel, @RequestParam(defaultValue = "0") int page,Principal principal) {


            theModel.addAttribute("principal",principal);
            Page<Blog> allBlogs = blogService.listAllBlogs(page);
            theModel.addAttribute("blogs", allBlogs);
        return "index";
    }

    @PostMapping("/save")
    public String saveBlog(@Valid @ModelAttribute("blog") Blog newBlog,BindingResult errors,@RequestParam(value="authorName")String authorName ,Principal principle,
    Model theModel){

        if(errors.hasErrors()) {
            theModel.addAttribute("author",authorName);
            return "createBlog";
        }

        Optional<User> author = userServiceImpl.findByUsername(authorName);
        if(author.isPresent()) {
            newBlog.setUser(author.get());
        }
        else{
            return "signIn";
        }
        newBlog.setIsPublished(true);
        

        System.out.println(newBlog.getBlogTags());
        String allTags[] = newBlog.getBlogTags().split(",");//check for commas in blogTags string
        for(String tag:allTags){
            Tag oldTag = tagService.findByTagName(tag);
            if(oldTag == null) {
                Tag newTag = new Tag();
                newTag.setTagName(tag);
                newBlog.getTags().add(newTag);
                newTag.getBlogs().add(newBlog);
            }
            else {
                oldTag.getBlogs().add(newBlog);
                newBlog.getTags().add(oldTag);
            }
        }

        blogService.save(newBlog);
        return "redirect:/";
    }

    @GetMapping("/displayBlog")
    public String displayBlog(@RequestParam("blogId") int blogId,Model theModel,Principal principal) {

        Comment comment = new Comment();
        if(principal == null) {
            comment.setUser(null);
            theModel.addAttribute("comment",comment);
            List<Comment> commentList = commentService.findByPost_Id(blogId);
            theModel.addAttribute("commentList",commentList);
            Blog showBlog = blogService.findById(blogId);
            Set<Tag> tags = showBlog.getTags();
            String blogTags = "";
            for(Tag tag:tags){
                blogTags += tag.getTagName();
            }
            showBlog.setBlogTags(blogTags);
            theModel.addAttribute("blog",showBlog);
            return "displayBlog";
        }
        User user = userServiceImpl.findByUsername(principal.getName()).get();
        comment.setUser(user);
        theModel.addAttribute("comment",comment);
        Blog showBlog = blogService.findById(blogId);
        List<Comment> commentList = commentService.findByPost_Id(blogId);
        theModel.addAttribute("commentList",commentList);
        Set<Tag> tags = showBlog.getTags();
        String blogTags = "";
        for(Tag tag:tags){
            blogTags += tag.getTagName();
        }
        showBlog.setBlogTags(blogTags);
        theModel.addAttribute("blog",showBlog);
        return "displayBlog";
    }

    @PostMapping("/displayBlog")
    public String saveComment(@Valid @ModelAttribute Comment comment, BindingResult errors,@RequestParam(value="blogId") int blog_Id, Model theModel) {

        if(errors.hasErrors()) {
            Blog showBlog = blogService.findById(blog_Id);
            List<Comment> list = commentService.findByPost_Id(blog_Id);
            Set<Tag> tags = showBlog.getTags();
            String blogTags = "";
            for(Tag tag:tags){
                blogTags += tag.getTagName();
            }
            showBlog.setBlogTags(blogTags);
            theModel.addAttribute("blog",showBlog);
            theModel.addAttribute("commentList",list);
            return "displayBlog";
        }

        logger.error("ERROR HERE");
        List<Comment> commentList = commentService.findByPost_Id(blog_Id);
        Blog blog = blogService.findById(blog_Id);
        comment.setBlog(blog);
        commentService.save(comment);


        return "redirect:/";
    }

    @GetMapping("/updateBlog")
    public String updateBlog(@RequestParam("blogId") int blogId,Model theModel,Principal principal) {
        System.out.println("IN UPDATE");
        if(principal == null) {
            return "signIn";
        }
        Blog updateBlog = blogService.findById(blogId);
        User user = userServiceImpl.findByUsername(principal.getName()).get();
        User blogUser = userServiceImpl.findByUsername(updateBlog.getUser().getUsername()).get();

        System.out.println(user.getUsername()+" "+blogUser.getUsername());

        Set<Tag> allTags = updateBlog.getTags();
//        User author = userServiceImpl.findByUsername(principal.getName()).get();
//        if(blogUser.getUsername().equals(user.getUsername())) {
        String blogTags = "";
        for (Tag tag : allTags) {
            blogTags += tag.getTagName() + ",";
            }
            updateBlog.setBlogTags(blogTags);

            if(blogUser.equals(user)) {
                theModel.addAttribute("author", user.getUsername());
                System.out.println(user.getUsername());
            }
            else{
                theModel.addAttribute("author",blogUser.getUsername());
                System.out.println(blogUser.getUsername());
            }
            theModel.addAttribute("blog", updateBlog);
            return "createBlog";

//        }
//        else{
//            return "signIn";
//        }
    }

    @Transactional
    @GetMapping("/deleteBlog")
    public String deleteBlog(@RequestParam("blogId") int blogId, Authentication principal, Model theModel) {
        System.out.println(blogId);
        if(principal == null) {
            return "signIn";
        }
        User user = userServiceImpl.findByUsername(principal.getName()).get();
        Blog blog = blogService.findById(blogId);
        String author = blog.getUser().getUsername();
        User blogUser = userServiceImpl.findByUsername(author).get();
        if(blogUser.getUsername().equals(user.getUsername()) || principal.getAuthorities().toString().contains("ROLE_ADMIN")){
            System.out.println("USER ALLOWED TO DELETE");
            commentService.deleteAllByBlog(blog);
            blogService.deleteById(blogId);
            return "forward:/";
        }
        else{
            System.out.println("NOT ALLOWED");
//            theModel.addAttribute("permit",false);
            return "signIn";
        }


    }

    @PostMapping("/saveAsDraft")
    public String saveAsDraft(@Valid @ModelAttribute("blog") Blog draft,BindingResult errors,@RequestParam(value="authorName")String authorName,
                              Model theModel,Principal principle) {
        if(errors.hasErrors()) {
            theModel.addAttribute("author",authorName);
            return "createBlog";
        }
        Optional<User> author = userServiceImpl.findByUsername(authorName);
        if(author.isPresent()) {
            draft.setUser(author.get());
        }
        else{
            logger.warn("NOT GETTING AUTHOR");
            return "signIn";
        }
        draft.setIsPublished(false);
        System.out.println(draft.getBlogTags());
        String allTags[] = draft.getBlogTags().split(",");//check for commas in blogTags string
        for(String tag:allTags){
            Tag oldTag = tagService.findByTagName(tag);
            if(oldTag == null) {
                Tag newTag = new Tag();
                newTag.setTagName(tag);
                draft.getTags().add(newTag);
                newTag.getBlogs().add(draft);
            }
            else {
                oldTag.getBlogs().add(draft);
                draft.getTags().add(oldTag);
            }
        }

        blogService.save(draft);
        return "redirect:/";
    }

    @GetMapping("/deleteComment")
    public String deleteComment(@RequestParam("blogId") int blogId,@RequestParam("commId") int commId,Model theModel,Authentication authentication) {
        Blog blog = blogService.findById(blogId);
        theModel.addAttribute("blog",blog);
        Comment comment = new Comment();
        theModel.addAttribute("comment",comment);
        commentService.deleteById(commId);
        return "forward:displayBlog";
    }

    @GetMapping("/updateComment")
    public String updateComment(@RequestParam(value = "commId") int commId,@RequestParam(value="blogId") int blogId,Model theModel) {
        Comment comment = commentService.findById(commId);
        Blog blog = blogService.findById(blogId);
        List<Comment> commentList = commentService.findByPost_Id(blogId);
        theModel.addAttribute("commentList",commentList);
        theModel.addAttribute("blog",blog);
        theModel.addAttribute("comment",comment);
        return "displayBlog";
    }

    @GetMapping("/search")
    public String search(@RequestParam(value="search") String search, @RequestParam(value="field") String field,@RequestParam(value="page",defaultValue = "0") int page,Model theModel) {

        Page<Blog> list = null;
        if(field.contains("Title")) {
            search += "%" + search + "%";
            list = blogService.listAllBlogsByTitle(search, page);
        }
        else if(field.contains("Author")){
            Optional<User> user = userServiceImpl.findByUsername(search);
            if(user.isPresent()) {
                list = blogService.findBlogsByUserAndIsPublishedTrue(user.get(), page);
                System.out.println(list.hasContent());
            }
        }
        else if(field.contains("Content")) {
            System.out.println("Inside content");
            search += "%" + search + "%";
            list = blogService.listAllBlogsByContent(search,page);
        }
        else if(field.contains("Tags")) {
            list = blogService.listAllBlogsByTags(true,search,page);
        }

        theModel.addAttribute("field",field);
        theModel.addAttribute("keyword",search);
        theModel.addAttribute("blogs",list);
        return "searchResults";
    }


    @GetMapping("/tag")
    public String allBlogsByTag(@RequestParam(value="tagId") int tagId,@RequestParam(defaultValue = "0") int page,Model theModel) {
        Tag findTag = tagService.findById(tagId);
        Page<Blog> listOfBlogs = blogService.listAllBlogsByTags(true,findTag.getTagName(),page);
        theModel.addAttribute("blogs",listOfBlogs);
        theModel.addAttribute("tagId",tagId);
        return "filterResults";
    }


    @GetMapping("/signIn")
    public String singIn(Principal principal) {
        if(principal != null) {
            return "redirect:/";
        }
        return "signIn";
    }

    @GetMapping("/userFeed")
    public String displayUserFeed(Principal principal,Model theModel,@RequestParam(defaultValue = "0")int page) {
        if(principal == null) {
            return "signIn";
        }
        User user = userServiceImpl.findByUsername(principal.getName()).get();
        Page<Blog> list = blogService.findBlogByUser(user,page);
        theModel.addAttribute("blogs",list);
        theModel.addAttribute("user",user);
//        theModel.addAttribute("feedType","userFeed");
        System.out.println(principal.getName());
        return "authorFeed";
    }

    @GetMapping("/blogsFeed")
    public String blogsFeed(@RequestParam(value="user") String username,Model theModel,@RequestParam(defaultValue = "0")int page) {
//            System.out.println(username+"99");
            User user = userServiceImpl.findByUsername(username).get();
            Page<Blog> list = blogService.findBlogsByUserAndIsPublishedTrue(user,page);
            theModel.addAttribute("blogs",list);
            theModel.addAttribute("user",user);
//            theModel.addAttribute("feedType","blogsFeed");
            return "authorFeed";
    }

    @GetMapping("/draftsFeed")
    public String draftsFeed(@RequestParam(value="user") String username,Model theModel,@RequestParam(defaultValue = "0")int page) {
            User user = userServiceImpl.findByUsername(username).get();
            Page<Blog> list = blogService.findBlogsByUserAndIsPublishedFalse(user,page);
            theModel.addAttribute("blogs",list);
            theModel.addAttribute("user",user);
            return "authorFeed";
    }

    @GetMapping("/AllBlogsFeed")
    public String allBlogsFeed(@RequestParam(value="user")String username,Model theModel,@RequestParam(defaultValue = "0")int page) {
        User user = userServiceImpl.findByUsername(username).get();
        Page<Blog> list = blogService.listAllBlogs(page);
        theModel.addAttribute("blogs",list);
        theModel.addAttribute("user",user);
        return "authorFeed";
    }

    @GetMapping("/AllDraftsFeed")
    public String allDraftsFeed(@RequestParam(value="user")String username,Model theModel,@RequestParam(defaultValue = "0")int page) {
        User user = userServiceImpl.findByUsername(username).get();
        Page<Blog> list = blogService.listAllDrafts(page);
        theModel.addAttribute("blogs",list);
        theModel.addAttribute("user",user);
        return "authorFeed";
    }






}
