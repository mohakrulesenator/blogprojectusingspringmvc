package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.entity.VerificationTokenClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationTokenClass,Long> {
    VerificationTokenClass findByUser(User user);
    VerificationTokenClass findByVerificationToken(String verificationToken);
    public void deleteByVerificationTokenId(long id);
}
