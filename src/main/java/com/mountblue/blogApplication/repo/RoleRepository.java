package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {
    public Role findByRole(String role);

}
